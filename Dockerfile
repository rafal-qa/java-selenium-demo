FROM openjdk:11

COPY gradle /usr/src/app/gradle
COPY src /usr/src/app/src
COPY build.gradle /usr/src/app
COPY gradlew /usr/src/app

WORKDIR /usr/src/app

CMD ["./gradlew", "test"]
