# Java Selenium demo project

Simple UI automation project using Java, Selenium, Gradle, Junit 5.

Thanks to running a browser in [Selenium Docker image](https://github.com/SeleniumHQ/docker-selenium):
* There is no need to download, configure, upgrade WebDriver binaries.
* All tests locally and on CI use the same browser and WebDriver versions.
* It's easy to test/debug on different browsers versions on one system.

## Requirements

* Java (tested on version 11)
* Docker

## Running tests

**Run Selenium Docker container**

```bash
docker run \
    -d \
    -p 4444:4444 \
    -p 5900:5900 \
    -e VNC_NO_PASSWORD=1 \
    --name selenium-grid \
    selenium/standalone-chrome-debug:3.141.59-20210607
```

To stop and remove container

```bash
docker rm -f selenium-grid
```

**Run tests**

```bash
./gradlew test
```

**To see a browser during the tests, connect to `localhost` in VNC client.** \
Selenium container uses VNC server to allow remote access to the system inside container.

### Running tests in Docker

It's possible to execute `./gradlew test` inside container.

Gitlab CI uses similar architecture, so you can use this way of running to check how it behaves before pushing changes.

`SELENIUM_GRID_ADDRESS` needs to be configured. \
When running locally (not on Gitlab CI), it's your local IP address different than `localhost`. Check by `ifconfig` command.

```bash
docker build -t java-selenium-demo .
docker run -it -e SELENIUM_GRID_ADDRESS=http://172.17.0.1:4444 java-selenium-demo
```

To access test reports, mount host directory in `docker run` command.

## Configuration

See `build.gradle` file for configurable options: `selenium_grid_address`, `base_url`, `window_size`.

## Screenshots

Use `ScreenshotUtils` class for taking screenshots and saving them to `build/screenshots/`.

### Saving screenshot on failure

`ScreenshotData` class is used for passing data from running tests to Junit's `TestWatcher`.
It's needed to take a screenshot always just before closing a browser and to save it only when `testFailed` (see `TestWithScreenshotExtension` class).

## Limitations

Since this is **simple** UI automation project, some features was not implemented, e.g.

* Won't work when `test.base_url` is on `localhost`, because Selenium container won't have an access to it. Possible solution is different on Linux and macOS, so explanation was omitted.
* Logs from Selenium container are not saved, but sometimes useful for debugging.
* `HEALTHCHECK` in Selenium container is not implemented. Tests should run only when Selenium server is up.
* No file downloads support. In current Gitlab CI pipeline architecture it's not possible to access files downloaded into Selenium container.

## Further improvements

When the project becomes bigger and more complex, consider following improvements:

* Run/Stop Selenium container using Docker Compose, Bash script, Makefile, Gradle, etc. It will make updating Selenium image version easier.
* Implement dependency injection using some library.
* Implement custom element class, instead of using `org.openqa.selenium.By` in `pages`. Perform waits and actions on this element.
* Parallel execution - see `maxParallelForks` in Gradle docs, but it's more complex than setting this property only.
