package com.rafalqa.pages;

import com.rafalqa.framework.Action;
import com.rafalqa.framework.Navigator;
import com.rafalqa.framework.Waiter;
import org.openqa.selenium.By;

public class HomePage {

    private final Navigator navigator = new Navigator();
    private final Waiter waiter = new Waiter();
    private final Action action = new Action();

    private final By logoWithoutLink = By.xpath("//div/img[@alt='Google']");
    private final By searchInput = By.xpath("//input[@name='q']");

    public void goTo() {
        navigator.openAppUrl("/?hl=en");
    }

    public void isAt() {
        waiter.isVisible(logoWithoutLink);
    }

    public void searchFor(String query) {
        action.typeInto(searchInput, query);
        action.pressEnter(searchInput);
    }
}
