package com.rafalqa.pages.components;

import com.rafalqa.framework.Action;
import com.rafalqa.framework.Waiter;
import org.openqa.selenium.By;

public class RecipesBoxComponent {

    private final Waiter waiter = new Waiter();
    private final Action action = new Action();

    private final By header = By.xpath("//g-section-with-header//h3[.='Recipes']");
    private final By items = By.xpath("//g-section-with-header//g-link");
    private final By showMoreButton = By.xpath("//g-section-with-header//span[.='Show more']");

    public void isActive() {
        waiter.isVisible(header);
    }

    public void hasDisplayedItems(int displayedItems) {
        waiter.elementsNumberIs(items, displayedItems);
        waiter.allElementsAreVisible(items);
    }

    public void showMore() {
        action.click(showMoreButton);
    }
}
