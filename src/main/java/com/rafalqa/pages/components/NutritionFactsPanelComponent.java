package com.rafalqa.pages.components;

import com.rafalqa.framework.Info;
import com.rafalqa.framework.Waiter;
import org.openqa.selenium.By;

public class NutritionFactsPanelComponent {

    private final Waiter waiter = new Waiter();
    private final Info info = new Info();

    private static final String NUTRITION_PANEL_XPATH
            = "//div[@data-attrid='kc:/food/food:nutrition']";

    private final By nutritionPanel = By.xpath(NUTRITION_PANEL_XPATH);

    private By singleNutrientValue(String nutrientName) {
        String xpath = String.format(
                "%s//tr/td/span[.='%s']/following-sibling::span",
                NUTRITION_PANEL_XPATH, nutrientName);
        return By.xpath(xpath);
    }

    public void isActive() {
        waiter.isVisible(nutritionPanel);
    }

    public String getValueOf(String nutrientName) {
        return info.getText(singleNutrientValue((nutrientName)));
    }
}
