package com.rafalqa.pages.components;

import com.rafalqa.framework.Info;
import com.rafalqa.framework.Waiter;
import org.openqa.selenium.By;

public class NutritionValueBoxComponent {

    private final Waiter waiter = new Waiter();
    private final Info info = new Info();

    private static final String BOX_BODY_XPATH
            = "//div[@class='kp-header']//div[starts-with(@data-attrid,'kc:/food/food:')]";

    private final By boxBody = By.xpath(BOX_BODY_XPATH);
    private final By valueHeader = By.xpath(BOX_BODY_XPATH + "//div[@role='heading']");

    public void isActive() {
        waiter.isVisible(boxBody);
    }

    public String getValue() {
        return info.getText(valueHeader);
    }
}
