package com.rafalqa.pages;

import com.rafalqa.framework.Action;
import com.rafalqa.framework.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;

public class CookiesOverlay {

    private final Waiter waiter = new Waiter();
    private final Action action = new Action();

    private final By agreeButton = By.xpath("//div[@role='dialog']//button/div[.='I agree']");

    public void agree() {
        action.click(agreeButton);
        waiter.isNotVisible(agreeButton);
    }

    public boolean wasDisplayed() {
        try {
            waiter.isVisible(agreeButton);
            return true;
        } catch (TimeoutException e) {
            return false;
        }
    }
}
