package com.rafalqa.pages;

import com.rafalqa.framework.Waiter;
import com.rafalqa.pages.components.NutritionFactsPanelComponent;
import com.rafalqa.pages.components.NutritionValueBoxComponent;
import com.rafalqa.pages.components.RecipesBoxComponent;
import org.openqa.selenium.By;

public class SearchResultPage {

    private final Waiter waiter = new Waiter();

    private final By resultStatsLabel = By.id("result-stats");

    public void isAt() {
        waiter.isVisible(resultStatsLabel);
    }

    public RecipesBoxComponent getRecipesBoxComponent() {
        return new RecipesBoxComponent();
    }

    public NutritionValueBoxComponent getNutritionValueBoxComponent() {
        return new NutritionValueBoxComponent();
    }

    public NutritionFactsPanelComponent getNutritionFactsPanelComponent() {
        return new NutritionFactsPanelComponent();
    }
}
