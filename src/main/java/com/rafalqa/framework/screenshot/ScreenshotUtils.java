package com.rafalqa.framework.screenshot;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;

public class ScreenshotUtils {

    public static File getAsTempFile(WebDriver driver) {
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    }

    public static void save(File file, String fileName) {
        String pathName = String.format("build/screenshots/%s.png", fileName);
        try {
            FileUtils.copyFile(file, new File(pathName));
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

}
