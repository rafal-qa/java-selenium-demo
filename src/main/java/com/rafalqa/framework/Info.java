package com.rafalqa.framework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Info {

    private final WebDriver driver = Driver.getDriver();

    public String getText(By locator) {
        return driver.findElement(locator).getText();
    }
}
