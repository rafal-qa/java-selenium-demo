package com.rafalqa.framework;

import org.openqa.selenium.WebDriver;

public class Navigator {

    private final WebDriver driver = Driver.getDriver();
    private final String baseUrl = System.getProperty("test.base_url");

    public void openAppUrl(String relativeUrl) {
        driver.get(baseUrl + relativeUrl);
    }
}
