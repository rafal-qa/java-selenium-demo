package com.rafalqa.framework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Waiter {

    private final WebDriver driver = Driver.getDriver();

    private int getTimeout() {
        return 5;
    }

    public void isVisible(By locator) {
        new WebDriverWait(driver, getTimeout())
                .until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void allElementsAreVisible(By locator) {
        new WebDriverWait(driver, getTimeout())
                .until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
    }

    public void isNotVisible(By locator) {
        new WebDriverWait(driver, getTimeout())
                .until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public void elementsNumberIs(By locator, int expectedNumber) {
        new WebDriverWait(driver, getTimeout())
                .until(ExpectedConditions.numberOfElementsToBe(locator, expectedNumber));
    }
}
