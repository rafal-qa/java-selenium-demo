package com.rafalqa.framework;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class Action {

    private final WebDriver driver = Driver.getDriver();

    public void click(By locator) {
        driver.findElement(locator).click();
    }

    public void typeInto(By locator, String value) {
        driver.findElement(locator).sendKeys(value);
    }

    public void pressEnter(By locator) {
        driver.findElement(locator).sendKeys(Keys.ENTER);
    }
}
