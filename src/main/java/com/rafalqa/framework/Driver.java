package com.rafalqa.framework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class Driver {

    private static WebDriver driver;
    private static final String seleniumGridAddress = System.getProperty("test.selenium_grid_address");
    private static final String windowSize = System.getProperty("test.window_size");

    private Driver() {}

    public static WebDriver getDriver() {
        if (driver == null) {
            driver = getSeleniumGridChromeDriver();
        }
        return driver;
    }

    private static WebDriver getSeleniumGridChromeDriver() {
        ChromeOptions options = new ChromeOptions();
        options.setCapability("browserName", "chrome");
        options.addArguments("--window-size=" + windowSize);

        // Disable using /dev/shm, because it can't be increased on Gitlab CI and causes browser crash
        // More: https://developers.google.com/web/tools/puppeteer/troubleshooting#tips
        options.addArguments("--disable-dev-shm-usage");

        try {
            return new RemoteWebDriver(new URL(seleniumGridAddress + "/wd/hub"), options);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    public static void close() {
        if (driver != null) {
            driver.close();
            driver = null;
        }
    }
}
