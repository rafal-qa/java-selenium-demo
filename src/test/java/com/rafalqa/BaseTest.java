package com.rafalqa;

import com.rafalqa.framework.Driver;
import com.rafalqa.framework.screenshot.ScreenshotData;
import com.rafalqa.framework.screenshot.ScreenshotUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(TestWithScreenshotExtension.class)
public class BaseTest {

    @BeforeEach
    public void startBrowser() {
        Driver.getDriver();
    }

    @AfterEach
    public void closeSession() {
        ScreenshotData.tempFile = ScreenshotUtils.getAsTempFile(Driver.getDriver());
        Driver.close();
    }
}
