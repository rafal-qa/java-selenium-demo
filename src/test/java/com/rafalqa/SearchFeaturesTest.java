package com.rafalqa;

import com.rafalqa.pages.CookiesOverlay;
import com.rafalqa.pages.HomePage;
import com.rafalqa.pages.SearchResultPage;
import com.rafalqa.pages.components.NutritionFactsPanelComponent;
import com.rafalqa.pages.components.NutritionValueBoxComponent;
import com.rafalqa.pages.components.RecipesBoxComponent;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SearchFeaturesTest extends BaseTest {

    private final HomePage homePage = new HomePage();
    private final CookiesOverlay cookiesOverlay = new CookiesOverlay();
    private final SearchResultPage searchResultPage = new SearchResultPage();

    @Test
    void userShouldBeAbleToBrowseRecipes() {
        openHomePage();
        homePage.searchFor("banana recipes");
        searchResultPage.isAt();

        RecipesBoxComponent recipesBox = searchResultPage.getRecipesBoxComponent();
        recipesBox.isActive();
        recipesBox.hasDisplayedItems(3);
        recipesBox.showMore();
        recipesBox.hasDisplayedItems(12);
    }

    @Test
    void userShouldBeAbleToCheckNutritionalValueOfFoodProduct() {
        openHomePage();
        homePage.searchFor("banana potassium");
        searchResultPage.isAt();

        NutritionValueBoxComponent nutritionValueBox = searchResultPage.getNutritionValueBoxComponent();
        nutritionValueBox.isActive();

        NutritionFactsPanelComponent nutritionFactsPanel = searchResultPage.getNutritionFactsPanelComponent();
        nutritionFactsPanel.isActive();

        String valueFromBox = nutritionValueBox.getValue();
        String valueFromPanel = nutritionFactsPanel.getValueOf("Potassium");
        assertThat(valueFromBox).isEqualTo(valueFromPanel);
    }

    private void openHomePage() {
        homePage.goTo();
        // Overlay with cookies info is displayed (or not) based on user IP
        // Probably is displayed only for users from EU
        if (cookiesOverlay.wasDisplayed()) {
            cookiesOverlay.agree();
        }
        homePage.isAt();
    }
}
