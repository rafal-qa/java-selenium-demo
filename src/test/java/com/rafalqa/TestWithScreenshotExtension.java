package com.rafalqa;

import com.rafalqa.framework.screenshot.ScreenshotUtils;
import com.rafalqa.framework.screenshot.ScreenshotData;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;

public class TestWithScreenshotExtension implements TestWatcher {

    @Override
    public void testFailed(ExtensionContext extensionContext, Throwable throwable) {
        String fileName = extensionContext.getDisplayName().replaceAll("[^a-zA-Z0-9]","_");
        ScreenshotUtils.save(ScreenshotData.tempFile, fileName);
    }

}
